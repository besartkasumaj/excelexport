<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class ExcelExport implements
    FromCollection,
    WithHeadings,
    ShouldAutoSize,
    WithEvents,
    WithCustomStartCell,
    WithColumnFormatting,
    WithColumnWidths
{
    /**
     * @return \Illuminate\Support\Collection
     */


    //This function allows you to specify a custom start cell
    public function startCell(): string
    {
        return 'A3';
    }


    //Hardcoded Data
    public function collection()
    {
        return new collection([
            [1, 1219919239914, '', 'Besart', 'Kasumaj', 161124241, 213, 'sae', '', '12 DE OCTUBRE', 'METROPOLITANA', 'Be Loyal', '', '', '']
        ]);
    }

    // Set multiple column formats
    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_NUMBER,
            'F' => NumberFormat::FORMAT_NUMBER,
        ];
    }


    public function registerEvents(): array
    {
        return [

            AfterSheet::class    => function (AfterSheet $event) {

                //Merge cells from A1:O1 and fill with data
                $cellMerge = 'A1:O1';
                $event->sheet->getDelegate()->mergeCells($cellMerge);
                $event->sheet->getDelegate()->setCellValue('A1', 'Driver Name: asd - Vendor: Mybox Express - Date: 2020-08-12 09:53:43 - Document Number: 120820200015');

                //Change A1 Cell font-weight to bold and position te center 
                $event->sheet->getStyle('A1')->applyFromArray([
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);

                //Border style thin, color black
                $event->sheet->getStyle('A3:O4')->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '0000000'],
                        ]
                    ]
                ]);

                //Insert Image to C4, with height 50, margins (x,y)(10,12)
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setPath(public_path('/code.JPG'));
                $drawing->setHeight(50);
                $drawing->setCoordinates('C4');
                $drawing->setOffsetX(10);
                $drawing->setOffsetY(12);
                $drawing->setWorksheet($event->sheet->getDelegate());

                //A3 headings, font-weight bold
                $event->sheet->getStyle('A3:O3')->applyFromArray([
                    'font' => [
                        'bold' => true,
                    ]
                ]);


                //Row 4, which contains the data of the collection. Changed height to 50
                $event->sheet->getDelegate()->getRowDimension(4)->setRowHeight(50);
            },

        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 8,
            'C' => 27,
        ];
    }


    public function headings(): array
    {
        return [
            'No',
            'XA#',
            'Barcode',
            'First Name',
            'Last Name',
            'Phone',
            'Address',
            'Address Reference',
            'Route',
            'Zone',
            'Area',
            'B2B',
            'Check',
            'Received By',
            'Signature'
        ];
    }
}
