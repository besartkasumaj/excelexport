<?php

namespace App\Http\Controllers;

use App\Exports\ExcelExport;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller 
{
    //Create excel file 
    public function export() 
    {
        return Excel::download(new ExcelExport, 'excel.xlsx');
    }

    public function importExportView()
    {
       return view('export');
    }
}